import 'source-map-support/register'
import fetch from 'node-fetch'

export interface ImdbType {
  "@type": string
  url?: string
}

export interface ImdbPerson extends ImdbType {
  "@type": "Person"
  name: string
  url: string
}

export interface ImdbRating extends ImdbType {
  bestRating: number
  ratingValue: number
  worstRating: number
}

export interface ImdbAggregateRating extends ImdbRating {
  ratingCount: number
}

export interface ImdbReview {
  "@type": string
  author: ImdbPerson
  dateCreated: string
  inLanguage: string
  itemReviewed: ImdbType
  name: string
  reviewBody: string
  reviewRating: string
}

export interface ImdbTrailer {
  "@type": "VideoObject"
  description: string
  embedUrl: string
  name: string
  thumbnail: ImdbImageObject
  thumbnailUrl: string
}

export interface ImdbImageObject {
  "@type": "ImageObject"
  contentUrl: string
}

export interface ImdbJson {
  "@context": string
  "@type": "Movie" | "TVSeries" | string
  actor: ImdbPerson[]
  aggregateRating: ImdbAggregateRating
  contentRating: string
  creator: ImdbType[]
  datePublished: string
  description: string
  director: ImdbPerson[]
  duration: string
  genre: string[]
  image: string
  keywords: string
  name: string
  review: ImdbReview
  trailer: ImdbTrailer
  url: string
}

export interface EpisodeData {
  url: string
  episodeNumber: number
  name: string
  releaseDate: Date
  sXeX: string
}

export interface ReleaseDate {
  date: Date
  name: string
}

export interface LocaleName {
  location: string
  name: string
}

export interface NameUrlPair {
  url: string
  name: string
}

export interface ImdbData extends ImdbJson {
  releaseDates: ReleaseDate[]
  localeNames: LocaleName[]
  countryOfOrigin: string
  language: string
  productionCompanies: NameUrlPair[]
  officialSite: NameUrlPair[]
  episodes: EpisodeData[]
}

const IMDB_BASE_URL = "https://www.imdb.com"
const IMDB_MOBILE_BASE_URL = "https://m.imdb.com"
const KNOWN_ORIGIN_SITES: [match: string, alias: string][] = [["Netflix Site", "Netflix"], ["Official Netflix", "Netflix"], ["Official Facebook", "Facebook"]]

// async function test(imdbId: string) {
//   const data = await getImdbInfo(imdbId)
//   console.log(data)
// }

// test('tt8772296')

export async function getImdbInfo(imdbId: string) {
  const regex = /tt(\d+)/g.exec(imdbId)
  if (!regex || regex.length < 2) {
    throw new Error(`${imdbId} is not a valid IMDb Id`)
  }
  imdbId = `tt${regex[1]}`
  const res = await fetch(`https://www.imdb.com/title/${imdbId}`)
  const text = await res.text()

  const innerJson = /<script type="application\/ld\+json">(.+?)<\/script>/gm.exec(text)

  if (!innerJson || innerJson.length < 2) {
    // Use some kind of backup HTML interpretter?
    throw new Error(`Couldn't find data object on page`)
  }

  const json: ImdbJson = JSON.parse(innerJson[1])

  json.duration = sanitizeRuntime(json.duration)

  const releaseInfo = await getReleaseInfo(imdbId)
  const more = getSupplementalDataFromHtml(text)

  const obj: ImdbData = {
    ...json,
    ...releaseInfo,
    ...more,
    episodes: []
  }

  if (json['@type'] === 'TVSeries') {
    obj.episodes = await getEpisodes(imdbId)
  }

  return obj
}

async function getReleaseInfo(imdbId: string) {
  const res = await fetch(`${IMDB_BASE_URL}/title/${imdbId}/releaseinfo`)
  const text = await res.text()
  const tables = text.split('<table class')

  const releases = tables[1].split('<tr class')

  const countries: ReleaseDate[] = []

  for (const row of releases) {
    const regex = /\n>(.+?)\n<\/a><\/td>\n<td class="release-date-item__date" align="right">(.+?)<\/td>/gm.exec(row)
    if (!regex) continue
    countries.push({
      name: regex[1],
      date: new Date(regex[2])
    })
  }

  const akas = tables[2].split('<tr class')

  const aliases: LocaleName[] = []

  for (const row of akas) {
    const regex = /<td class="aka-item__name">(.+?)<\/td>\n\s+<td class="aka-item__title">(.+?)<\/td>/gm.exec(row)
    if (!regex) continue
    aliases.push({
      location: regex[1].trim(),
      name: regex[2]
    })
  }

  return {
    releaseDates: countries,
    localeNames: aliases
  }
}

async function getEpisodes(imdbId: string) {
  const episodes: EpisodeData[] = []

  const res = await fetch(`${IMDB_MOBILE_BASE_URL}/title/${imdbId}/episodes`)
  const text = await res.text()

  const seasonCount = text.split(/season_number="\d+">\d+</g).length - 1

  for (let i = 1; i <= seasonCount; i++) {
    const res = await fetch(`${IMDB_MOBILE_BASE_URL}/title/${imdbId}/episodes/?season=${i}`)
    const text = await res.text()

    const idListRegex = /div id="eplist"(.+?)<br \/>/gm.exec(text.replace(/\n/g, ' '))

    if (!idListRegex) {
      return []
    }

    const seasonEpisodes = interpretEpisodeDataForPage(idListRegex[1], i)
    episodes.splice(episodes.length, 0, ...seasonEpisodes)
  }

  return episodes
}

function interpretEpisodeDataForPage(html: string, seasonNumber: number) {
  const episodes = []

  const rows = html.split('wtw-option-standalone')

  for (const row of rows) {

    //<a href="/title/tt15215398?ref_=m_ttep_ep_ep5" class="btn-full" > <span class="text-large episode-list__title"> 5. <strong class="episode-list__title-text">A New Life</strong> </span> 21 Feb. 2022 </a>                <div class="
    const regex = /<a href="(.+?)" class="btn-full" > <span class="text-large episode-list__title"> (\d+)\. <strong class="episode-list__title-text">(.+?)<\/strong> <\/span>(?:.+?&nbsp;&nbsp;)?(.+?)<\/a>/gm.exec(row)
    if (!regex) continue

    episodes.push({
      url: `${IMDB_BASE_URL}${regex[1]}`,
      episodeNumber: Number(regex[2]),
      name: regex[3],
      releaseDate: new Date(regex[4]),
      sXeX: `S${seasonNumber.toString().padStart(2, '0')}E${regex[2].padStart(2, '0')}`
    } as EpisodeData)
  }

  return episodes
}

/** Get data that we can't get from the JSON on the main page
 * Currently only fetches data inside "Details" sub section
 * Missing sections: Videos, Photos, Top Cast, More like this, Storyline, Trivia, User reviews
 */
function getSupplementalDataFromHtml(rawHtml: string) {
  const detailsSection = /section data-testid="Details"(.+?)<\/section>/gm.exec(rawHtml)
  // What we're interested in here:
  // Country of origin
  // Official site
  // Language
  // Production companies

  if (!detailsSection) {
    return {
      countryOfOrigin: 'Unknown',
      language: 'Unknown',
      productionCompanies: [],
      officialSite: []
    }
  }

  const cooRegex = /Country of origin.+?cn">(.+?)<\/a>/gm.exec(detailsSection[1])
  const langRegex = /Language.+?ln">(.+?)<\/a>/gm.exec(detailsSection[1])

  return {
    countryOfOrigin: cooRegex?.[1] || 'Unknown',
    language: langRegex?.[1] || 'Unknown',
    productionCompanies: getProductionCompanies(detailsSection[1]),
    officialSite: getOfficialSites(detailsSection[1])
  }
}

function getOfficialSites(detailsHtml: string) {
  const sitesRegex = /Official site.+?(<li.+?<\/a><\/li>)<\/ul>/gm.exec(detailsHtml)
  const officialSites: NameUrlPair[] = []

  if (!sitesRegex) {
    // Oops
    return []
  }

  const sites = sitesRegex[1].split('<li role')

  for (const site of sites) {
    const regex = /href="(.+?)" target="_blank">(.+?)<\/a>/gm.exec(site)
    if (!regex) continue
    officialSites.push({
      url: `${regex[1]}`,
      name: sanitizeKnownSite(regex[2])
    })
  }

  return officialSites
}

function getProductionCompanies(detailsHtml: string) {
  const prodRegex = /Production companies.+?(<li.+?<\/a><\/li>)<\/ul>/gm.exec(detailsHtml)
  const productionCompanies: NameUrlPair[] = []

  if (!prodRegex) {
    // Oops
    return []
  }

  const companies = prodRegex[1].split('<li role')

  for (const comp of companies) {
    const regex = /href="(.+?)">(.+?)<\/a>/gm.exec(comp)
    if (!regex) continue
    productionCompanies.push({
      url: `${IMDB_BASE_URL}${regex[1]}`,
      name: regex[2]
    })
  }

  return productionCompanies
}

/** Replaces the value with a known alias (example: Netflix Site -> Netflix) */
function sanitizeKnownSite(knownSite: string) {
  const found = KNOWN_ORIGIN_SITES.find(o => o[0] === knownSite)
  if (!found) {
    return knownSite
  }
  return found[1]
}

/** Converts PTXHXM to HH:MM format */
function sanitizeRuntime(rawRuntime: string) {
  const regex = /PT(\d+)H(\d+)M/gm.exec(rawRuntime)
  if (regex && regex.length > 2) {
    return `${regex[1].padStart(2, '0')}:${regex[2].padStart(2, '0')}`
  }

  return rawRuntime
}